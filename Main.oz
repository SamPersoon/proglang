functor 
import 
   Player 
   Referee
define 
   local RedPlayer BluePlayer Ref in 
      RedPlayer = {Player.createPlayer}
      BluePlayer = {Player.createPlayer}
      Ref = {Referee.createReferee RedPlayer BluePlayer}
      {Send Ref startGame}
   end      
end

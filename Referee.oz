functor 
export 
   createReferee: CreateReferee
import 
   System(printInfo:Print show:Show)
define 
    /**
    * Create a new port which handles the message stream.
    *
    * State: The current state 
    * Fun: The state transition function
    */
   fun {NewPortObject State Fun}
      local Sin in 
         thread {FoldL Sin Fun State _} end 
         {NewPort Sin}
      end 
   end 

   /*
    * The recursive play function
    * State: the current state of the game
   */
   proc {Play State}
      local Move in
         if State.turn == red then
           {Send State.redplayer generateMove(State.moves State.turn State.turnsUntilSwap Move)}
         else 
           {Send State.blueplayer generateMove(State.moves State.turn State.turnsUntilSwap Move)}
         end
         case Move
         of move(row:Row column:Column color:Color) then
            if State.turn == red then
               {Print "\n"#State.redplayerstring#" wants to place a "#Color#" tile on ("#Row#", "#Column#")\n"} 
            else
               {Print "\n"#State.blueplayerstring#" wants to place a "#Color#" tile on ("#Row#", "#Column#")\n"} 
            end
            if {Not {CheckMove State Move}} then
               /*Player did not cheat twice yet*/
               if State.strikes < 1 then
                  if State.turn == red then
                     {Print "Please chooose a tile that is valid, "#State.redplayerstring#"!\n"}
                  else
                     {Print "Please chooose a tile that is valid, "#State.blueplayerstring#"!\n"}
                  end
                  {Play {SetStrikes State 1}}
               /*Player cheated twice*/
               else
                  if State.turn == red then
                     {Print "\n"#State.blueplayerstring#" wins, "#State.redplayerstring#" is a cheater!\n"}
                  else
                     {Print "\n"#State.redplayerstring#" wins, "#State.blueplayerstring#" is a cheater!\n"}
                  end
               end
            else
               
               {Print "\n"}
               local UpdatedState in 
                  UpdatedState = {UpdateState {CheckSwap State Move} Move}
                  {PrintBoard UpdatedState.moves}
                  if {GameHasEnded UpdatedState} then
                     if Color == blue then
                        {Print "\n"#State.blueplayerstring#" wins, go "#State.blueplayerstring#"!"}
                     else
                        {Print "\n"#State.redplayerstring#" wins, go "#State.redplayerstring#"!"}
                     end
                  else
                     {Play UpdatedState}
                  end
               end
            end
         else
            {Print "\n"}
            {Show Move}
            {Print "Move should be `move(row: _ column: _ color: _)`"} 
         end
      end 
   end

   /**
    * Create a new referee
    *
    * PlayerWhite: The port of the red player 
    * PlayerBlack: The port of the blue player
    */
   fun {CreateReferee PlayerRed PlayerBlue}
      {NewPortObject state(moves: nil redplayer: PlayerRed redplayerstring: "Red" blueplayer: PlayerBlue blueplayerstring: "Blue" turn: red strikes: 0 turncount: 0 turnsUntilSwap: 0 unions: nil) fun {$ State Msg}
         case Msg 
         of startGame() then
            {Print "\nGame started\n\n"}
            {Play State}
            State
         else
            {Print 'message unknown\n'}
            State
         end 
      end}  
   end 
   
   /*
    * Check if a swap needs to occur
    * State: the current state
    * Move: the new move
   */
   fun {CheckSwap State Move}
      if State.turncount == 0 then
         local SwapTurn in
            {Send State.blueplayer swapRequest(Move SwapTurn)}
            {AddTurnsUntilSwap State SwapTurn} 
         end
      else
         State
      end
   end

   /*
    * Add turnsuntilswap to state
    * State: the current state
    * SwapTurn: Number untils swap, only values between [1,6] are considered
   */
   fun {AddTurnsUntilSwap State SwapTurn}
      if {And SwapTurn > 0 SwapTurn < 7} then
         {Print "The blue player decided to swap after turn "#SwapTurn#"\n\n"}
         {Record.adjoinAt State turnsUntilSwap SwapTurn}
      else
         {Print "The blue player decided not to swap colors\n\n"}
         {Record.adjoinAt State turnsUntilSwap ~1}
      end
   end


   /*
    * Check if the game has ended
    * State: The current state
   */
   fun {GameHasEnded State}
     {GameHasEndedUnions State.unions} 
   end

   /*
    * Check if the game has ended using disjoint sets
    * Unions: the disjoint sets
   */
   fun {GameHasEndedUnions Unions}
      case Unions of 
      X|Xs then
         if {ListHasBothEnds X false false} then
            true
         else
            {GameHasEndedUnions Xs}
         end
      [] nil then
         false 
      end
   end
   
   /*
    * Check if list has both sides in it
    * Moves: the list of moves
    * Begin: true if begin already found
    * End: true if end already found
   */
   fun {ListHasBothEnds Moves Begin End}
      if {And Begin End} then
         true
      else
         case Moves of 
         Move|Xs then
            if Move.color == red then
               {ListHasBothEnds Xs {Or Begin Move.row == 1} {Or End Move.row == 11}}
            else 
               {ListHasBothEnds Xs {Or Begin Move.column == 1} {Or End Move.column == 11}}
            end
         [] nil then
            false
         end
      end
   end 

   /*
    * Updates the state
    * State: the previous state
    * Move: the new move
   */
   fun {UpdateState State Move}
      {UpdateTurnCount
         {UpdateMoves
            {SetStrikes
               {ChangeTurn 
                  {Record.adjoinAt State unions {UpdateUnions State.unions Move}} 
               }
               0
            }
            Move
         }
      }
   end

   /*
    * Update turncount in State and swap players if neccessary
    * State: the current state
   */
   fun {UpdateTurnCount State}
      local TempState in
         TempState = {Record.adjoinAt 
                        {Record.adjoinAt State turncount State.turncount + 1}
                        turnsUntilSwap 
                        State.turnsUntilSwap - 1
                     }
         if TempState.turnsUntilSwap == 0 then
            {Print "Blue now plays with red and Red plays with blue\n\n"}
            local PlayerBlue PlayerRed in
               PlayerBlue = TempState.blueplayer
               PlayerRed = TempState.redplayer
               {Record.adjoinAt 
                  {Record.adjoinAt
                     {Record.adjoinAt 
                        {Record.adjoinAt TempState redplayer PlayerBlue}
                        blueplayer 
                        PlayerRed
                     }
                     blueplayerstring "Red"
                  }
               redplayerstring "Blue"
               }
            end
         else
            TempState
         end
      end
   end
   
   /*
    * Update the disjoint sets with new move (potentially join some sets) 
    * Unions: the current disjoint sets
    * Move: the new move
   */
   fun {UpdateUnions Unions Move}
      {UpdateUnionsRecursive Unions Move|nil nil Move}
   end

   /*
    * Update the disjoint sets with the new move
    * Unions: The unions that are not checked yet
    * ChangedAccumulator: The disjoint set with the new move in
    * UnchangedAccumulator: The list of Disjoint sets that are not changed
    * Move: the new move
   */   
   fun {UpdateUnionsRecursive Unions ChangedAccumulator UnChangedAccumulator Move}
      case Unions 
      of X|Xs then
         if {HasNeighborInList X Move} then
            {UpdateUnionsRecursive Xs {List.flatten X|ChangedAccumulator} UnChangedAccumulator Move}
         else
            {UpdateUnionsRecursive Xs ChangedAccumulator X|UnChangedAccumulator Move}
         end
      [] nil then
         ChangedAccumulator | UnChangedAccumulator
      end
   end

   /*
    * Check if a move has neighbors in a given list
    * MoveList: the list to check
    * Move: the move to check
    */
   fun {HasNeighborInList MoveList Move}
      case MoveList
      of X|Xs then
         if {IsNeighbor X Move} then
            true
         else
            {HasNeighborInList Xs Move} 
         end
      [] nil then
         false
      end
   end

   /*
    * Check if two moves are neighbors and sharing color
    * Move: the first move
    * OtherMove: the second move
    */
   fun {IsNeighbor Move OtherMove}
      if Move.color == OtherMove.color then
         if {Or
               {And {Number.abs Move.row - OtherMove.row} == 1 Move.column == OtherMove.column}
               {And {Number.abs Move.column - OtherMove.column} == 1 Move.row == OtherMove.row}
            } 
         then
            true
         else 
            if {Or
                  {And Move.row - 1 == OtherMove.row Move.column + 1 == OtherMove.column}
                  {And Move.row + 1 == OtherMove.row Move.column - 1 == OtherMove.column}
               } 
            then
               true 
            else
               false
            end
         end
      else
         false
      end
      
   end

   /* 
    * Update the move list in the given state
    * GameState: the current state
    * Move: the new move
    */ 
   fun {UpdateMoves GameState Move}
      {Record.adjoinAt GameState moves Move|GameState.moves}
   end 

   /* 
    * Update the strikecount in the given state
    * GameState: the current state
    * Strikes: new number of strikes
    */
   fun {SetStrikes GameState Strikes}
      {Record.adjoinAt GameState strikes Strikes} 
   end
   
   /* Switch turns in the given state
    * GameState: the current state
    */
   fun {ChangeTurn GameState}
      if GameState.turn == red then
         {Record.adjoinAt GameState turn blue}
      else
         {Record.adjoinAt GameState turn red}
      end
   end

   /* 
    * Check if a move is in range and does not exist
    * GameState: the current state
    * Move: the move to check
    */
   fun {CheckMove GameState Move}
      {And
         {And
            {InRange Move.row Move.column}
            Move.color == GameState.turn
         }
         {Not {MoveExists GameState.moves Move}}
      }
   end

   /*
    * Checks if the move exists
    * Moves: list of exisitng moves
    * Move: the move to check
    */
   fun {MoveExists Moves Move}
      case Moves of 
      X|Xs then
         if {And Move.row == X.row Move.column == X.column}
         then 
            true
         else
            {MoveExists Xs Move}
         end
      [] nil then
         false
      end
   end

   /*
    * checks if row and column are in range [1,11]
    * Row: the row
    * Col: the column
    */
   fun {InRange Row Col}
      {And 
         {And Row >= 1 Col >=1} 
         {And Row =< 11 Col =< 11}
      }
   end

   /*
    * Prints the board using the given board
    * Board: the board to use
    */
   proc {PrintUsingBoardRepresentation Board}
      for Y in 1..11 do
         for I in 0..Y-1 do
            {Print '  '}
         end
         for X in 1..11 do
            local CurrentChar in
               CurrentChar = {List.nth {List.nth Board X} Y}
               /* {Browse CurrentChar} */
               if {Value.isFree CurrentChar} then
                  {Print ' -  '}
               else
                  if CurrentChar == blue then
                     {Print ' b  '}
                  else
                     {Print ' r  '}
                  end
                  
               end
            end
         end
         {Print '\n'}
      end
   end
                                                                                                        

   /*
    * Fills the board with the correct tiles
    * Moves: the existing moves
    * Board: the board
    */
   fun {FillBoard Moves Board}
      case Moves of move(row:Y column:X color:C)|Xs then
         {List.nth {List.nth Board X} Y} = C
         {FillBoard Xs Board}
      [] nil then
         Board
      end
   end

  /*
   * Prints the board
   * Moves: the existing moves
   */ 
   proc {PrintBoard Moves}
      local NewBoard in
         {List.make 11 NewBoard}
         for I in 1..11 do
            {List.make 11 {List.nth NewBoard I $}}
         end
         {PrintUsingBoardRepresentation {FillBoard Moves NewBoard}}
      end
   end
   
end

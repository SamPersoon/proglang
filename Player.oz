functor 
export
   createPlayer: CreatePlayer
import 
   System(printInfo: Print )
define

   /**
   * Create a new port which handles the message stream.
   *
   * Proc: The procedure which handles the messages in the stream
   */
   fun {CreatePort Proc}
      Sin in 
         thread for Msg in Sin do {Proc Msg} end 
      end 
      {NewPort Sin}
   end 

   /**
    * Create a new player 
    *
    * Referee: The port of the referee
    * Color: The color of the player
    */
   fun {CreatePlayer}
      {CreatePort proc {$ Msg}
         case Msg 
         of generateMove(ExistingMoves Color TurnsUntilSwap ?Move) then
            Move = {GenerateMove ExistingMoves Color TurnsUntilSwap}
         [] swapRequest(Move ?TurnsUntilSwap) then
            if {And 
                  {And Move.column > 5 Move.column < 12}
                  {And Move.row > 1 Move.row < 11}
               }
            then
               TurnsUntilSwap = 1
            else
               TurnsUntilSwap = ~1
            end
         else
            {Print 'Playermessage unknown'}
         end
      end} 
   end

   /*
    * Generate a new move
    * ExistingMoves: List of existing moves
    * Color: the color of the new move
    * TurnsUntilSwap: an integer of when a swap will occur, ignored if < 0
   */
   fun {GenerateMove ExistingMoves Color TurnsUntilSwap}
      if TurnsUntilSwap > 0 then
         {GenerateBadMove ExistingMoves Color}
      else
         {GenerateGoodMove ExistingMoves Color}
      end
   end
   
   /*
    * Generate a bad move, used when swapping is stalled
    * ExistingMoves: List of existing moves
    * Color: Color of the new move
   */
   fun {GenerateBadMove ExistingMoves Color}
      if Color == red then
         {GenerateMoveTop ExistingMoves move(row: 1 column: 1 color: red)}
      else
         {GenerateMoveLeft ExistingMoves move(row: 1 column: 1 color: blue)}
      end
   end

   /*
    * Generate a bad move on the top side of the board 
   */
   fun {GenerateMoveTop ExistingMoves Move}
      if {Not {MoveExists ExistingMoves Move false} } then
         Move
      else
         {GenerateMoveTop ExistingMoves {Record.adjoinAt Move row Move.row + 1}}
      end
   end
   
   /*
    * Generate a bad move, on the left side of the board
    * ExistingMoves: List of existing moves
    * Color: Color of the new move
   */
   fun {GenerateMoveLeft ExistingMoves Move}
      if {Not {MoveExists ExistingMoves Move false} } then
         Move
      else
         {GenerateMoveTop ExistingMoves {Record.adjoinAt Move column Move.column + 1}}
      end
   end
   
   /*
    * Check if a move exists
    * Moves: List of moves to check
    * Move: The new move
    * CheckColor: True if the color of the move also has to match
   */
   fun {MoveExists Moves Move CheckColor}
      case Moves of 
      X|Xs then
         if {And 
               {And Move.row == X.row Move.column == X.column}
               {Not {And CheckColor Move.color \= X.color}}  
            }
         then 
            true
         else
            {MoveExists Xs Move CheckColor}
         end
      [] nil then
         false
      end
   end

   /*
    * Generate a good move (when swapping already happened)
    * ExistingMoves: list of existing moves
    * Color: the new color
   */
   fun {GenerateGoodMove ExistingMoves Color}
      case ExistingMoves of
      X|Xs then
         local FixedMove in
            FixedMove = {Endangers X Xs Color}
            case FixedMove of
            nil then
               if {List.length ExistingMoves} == 1 then
                  move(row: 2 column : 8 color: blue)
               else
                  {GenerateStronglyConnectedPath ExistingMoves ExistingMoves Color}
               end
            else
               FixedMove
            end
         end
      [] nil then
         move(row: 5 column: 5 color: Color)
      end
   end

   /*
    * Try to generate strongly connected tiles
    * ExistingMoves: list of existing moves
    * UsingMoves: list of moves we can try strongly connect to
    * Color: the color of the new move
   */
   fun {GenerateStronglyConnectedPath ExistingMoves UsingMoves Color}
      case UsingMoves of
      _|Y|_ then
         if Color == red then
            {GenerateRedStrongPath Y ExistingMoves UsingMoves true}
         else
            {GenerateBlueStrongPath Y ExistingMoves UsingMoves true}
         end
      else
         {GenerateFirstAbleMove ExistingMoves move(row: 1 column:1 color: Color)}
      end
   end

   /*
    * Generate the first move were no tile exists
    * ExistingMoves: list of existing moves
    * Move: The current move tried
   */
   fun {GenerateFirstAbleMove ExistingMoves Move}
      if {MoveExists ExistingMoves Move false} then
         if Move.column == 11 then
            {GenerateFirstAbleMove ExistingMoves move(row: Move.row + 1 column: 1 color: Move.color)}
         else
            {GenerateFirstAbleMove ExistingMoves move(row: Move.row column :Move.column + 1 color: Move.color)}
         end
      else
         Move
      end
   end

   /*
    * Generate a strongly connected path for red
    * Move: the previous move of red
    * ExistingMoves: list of existing moves
    * UsingMoves: list of moves we can try to strongly connect
    * Up: True if we are trying to create a path to the upperside of the board, false if lower side
   */
   fun {GenerateRedStrongPath Move ExistingMoves UsingMoves Up}
      if {And Move.row < 3 Up} then
         {GenerateRedStrongPath Move ExistingMoves UsingMoves false}
      elseif {And Move.row > 9 {Not Up}} then
         {GenerateRedStrongPath Move ExistingMoves UsingMoves true}
      else
         
         if Up then
            if {MoveNotExistsAndInRange ExistingMoves move(row: Move.row - 2 column: Move.column + 1 color: red) false} then
               move(row: Move.row - 2 column: Move.column + 1 color: red)
            elseif {MoveNotExistsAndInRange ExistingMoves move(row: Move.row - 1 column: Move.column - 1 color: red) false} then
               move(row: Move.row - 1 column: Move.column - 1 color: red)
            elseif {MoveNotExistsAndInRange ExistingMoves move(row: Move.row - 1 column: Move.column + 2 color: red) false} then
               move(row: Move.row - 1 column: Move.column + 2 color: red)
            else
               {GenerateStronglyConnectedPath ExistingMoves {List.drop UsingMoves 2} red}
            end
         else
            if {MoveNotExistsAndInRange ExistingMoves move(row: Move.row + 2 column: Move.column - 1 color: red) false} then
               move(row: Move.row + 2 column: Move.column - 1 color:red)
            elseif {MoveNotExistsAndInRange ExistingMoves move(row: Move.row + 1 column: Move.column + 1 color: red) false} then
               move(row: Move.row + 1 column: Move.column + 1 color: red)
            elseif {MoveNotExistsAndInRange ExistingMoves move(row: Move.row + 1 column: Move.column - 2 color: red) false} then
               move(row: Move.row + 1 column: Move.column - 2 color: red)
            else
               {GenerateStronglyConnectedPath ExistingMoves {List.drop UsingMoves 2} red}
            end
         end
      end
   end
   
   /*
    * Generate a strongly connected path for blue
    * Move: the previous move of blue
    * ExistingMoves: list of existing moves
    * UsingMoves: list of moves we can try to strongly connect
    * Up: True if we are trying to create a path to the left side of the board, false if right side
   */
   fun {GenerateBlueStrongPath Move ExistingMoves UsingMoves Left}
       if {And Move.column < 3 Left} then
         {GenerateBlueStrongPath Move ExistingMoves UsingMoves false}
      elseif {And Move.column > 9 {Not Left}} then
         {GenerateBlueStrongPath Move ExistingMoves UsingMoves true}
      else
         
         if {Not Left} then
            if {MoveNotExistsAndInRange ExistingMoves move(row: Move.row - 2 column: Move.column + 1 color: blue) false} then
               move(row: Move.row - 2 column: Move.column + 1 color: blue)
            elseif {MoveNotExistsAndInRange ExistingMoves move(row: Move.row + 1 column: Move.column + 1 color: blue) false} then
               move(row: Move.row + 1 column: Move.column + 1 color: blue)
            elseif {MoveNotExistsAndInRange ExistingMoves move(row: Move.row - 1 column: Move.column + 2 color: blue) false} then
               move(row: Move.row - 1 column: Move.column + 2 color: blue)
            else
               {GenerateStronglyConnectedPath ExistingMoves {List.drop UsingMoves 2} blue}
            end
         else
            if {MoveNotExistsAndInRange ExistingMoves move(row: Move.row + 2 column: Move.column - 1 color: blue) false} then
               move(row: Move.row + 2 column: Move.column - 1 color:blue)
            elseif {MoveNotExistsAndInRange ExistingMoves move(row: Move.row - 1 column: Move.column - 1 color: blue) false} then
               move(row: Move.row - 1 column: Move.column - 1 color: blue)
            elseif {MoveNotExistsAndInRange ExistingMoves move(row: Move.row + 1 column: Move.column - 2 color: blue) false} then
               move(row: Move.row + 1 column: Move.column - 2 color: blue)
            else
               {GenerateStronglyConnectedPath ExistingMoves {List.drop UsingMoves 2} blue}
            end
         end
      end
   end
 
   /*
    * Check if a move does not exists and is also in range of the board
    * ExistingMoves: list of existing moves
    * Move: Move to check
    * CheckColor: True if color also has to match
    */ 
   fun {MoveNotExistsAndInRange  ExistingMoves Move CheckColor}
      {And
         {Not {MoveExists ExistingMoves Move CheckColor}}
         {InRange Move.row Move.column}
      }
   end

   
   /*
    * Check all endangered strong connected tiles
    * Move: The last move
    * ExistingMoves: list of existing Moves
    * Color: color of the new move
    */
   fun {Endangers Move ExistingMoves Color} 
      if 
         {CheckEndangered 
            move(row: Move.row - 1 column: Move.column color: Color)
            move(row: Move.row + 1 column: Move.column - 1 color: Color)
            move(row: Move.row column: Move.column - 1 color: Color)
            ExistingMoves
         } 
      then
         move(row: Move.row column: Move.column - 1 color: Color)
      elseif 
         {CheckEndangered 
            move(row: Move.row - 1 column: Move.column color: Color)
            move(row: Move.row column: Move.column + 1 color: Color)
            move(row: Move.row - 1 column: Move.column + 1 color: Color)
            ExistingMoves
         } 
      then
         move(row: Move.row - 1 column: Move.column + 1 color: Color)
      elseif
         {CheckEndangered 
            move(row: Move.row column: Move.column + 1 color: Color)
            move(row: Move.row + 1 column: Move.column - 1 color: Color)
            move(row: Move.row + 1 column: Move.column color: Color)
            ExistingMoves
         }
      then
            move(row: Move.row + 1 column: Move.column color: Color)
      elseif 
         {CheckEndangered 
            move(row: Move.row column: Move.column - 1 color: Color)
            move(row: Move.row - 1 column: Move.column + 1 color: Color)
            move(row: Move.row - 1 column: Move.column color: Color)
            ExistingMoves
         }
      then
            move(row: Move.row - 1 column: Move.column color: Color)
      elseif
         {CheckEndangered 
            move(row: Move.row column: Move.column - 1 color: Color)
            move(row: Move.row + 1 column: Move.column color: Color)
            move(row: Move.row + 1 column: Move.column - 1 color: Color)
            ExistingMoves
         }
      then
            move(row: Move.row + 1 column: Move.column - 1 color: Color)
      elseif
         {CheckEndangered 
            move(row: Move.row + 1 column: Move.column color: Color)
            move(row: Move.row - 1 column: Move.column  + 1 color: Color)
            move(row: Move.row column: Move.column + 1 color: Color)
            ExistingMoves
         }
      then
            move(row: Move.row column: Move.column + 1 color: Color)
      else 
         {CheckEndangeredSides Move ExistingMoves Color}
      end
   end

   /*
    * Check for endangered strongly connected paths on the sides
    * Move: the last move
    * ExistingMoves: List of existing moves
    * Color: Color of new move
    *
    */
   fun {CheckEndangeredSides Move ExistingMoves Color}
      if {And
            {And Color == red Move.row == 1}
            {And Move.column > 1 Move.column < 11}
         }
      then 
         if {And
               {MoveExists ExistingMoves move(row: Move.row + 1 column: Move.column color: red) false}
               {Not {MoveExists ExistingMoves move(row: Move.row column: Move.column + 1 color: red) false}}
            }
         then
            move(row: Move.row column: Move.column + 1 color: red)
         elseif 
            {And
               {MoveExists ExistingMoves move(row: Move.row + 1 column: Move.column - 1 color: red) false}
               {Not {MoveExists ExistingMoves move(row: Move.row column: Move.column - 1 color: red) false}}
            }
         then
            move(row: Move.row column: Move.column - 1 color: red)
         else 
            nil
         end
      elseif 
         {And
            {And Color == red Move.row == 11}
            {And Move.column > 1 Move.column < 11}
         }
      then 
         if {And
               {MoveExists ExistingMoves move(row: Move.row - 1 column: Move.column color: red) false}
               {Not {MoveExists ExistingMoves move(row: Move.row column: Move.column - 1 color: red) false}}
            }
         then
            move(row: Move.row column: Move.column - 1 color: red)
         elseif 
            {And
               {MoveExists ExistingMoves move(row: Move.row - 1 column: Move.column + 1 color: red) false}
               {Not {MoveExists ExistingMoves move(row: Move.row column: Move.column + 1 color: red) false}}
            }
         then
            move(row: Move.row column: Move.column + 1 color: red)
         else 
            nil
         end
      elseif
         {And
            {And Color == blue Move.column == 1}
            {And Move.row > 1 Move.row < 11}
         }
      then 
         if {And
               {MoveExists ExistingMoves move(row: Move.row column: Move.column + 1 color: blue) false}
               {Not {MoveExists ExistingMoves move(row: Move.row +1 column: Move.column color: blue) false}}
            }
         then
            move(row: Move.row + 1 column: Move.column color:blue)
         elseif 
            {And
               {MoveExists ExistingMoves move(row: Move.row - 1 column: Move.column + 1 color: blue) false}
               {Not {MoveExists ExistingMoves move(row: Move.row -1 column: Move.column  color:blue) false}}
            }
         then
            move(row: Move.row - 1 column: Move.column color:blue)
         else 
            nil
         end
      elseif
         {And
            {And Color == blue Move.column == 11}
            {And Move.row > 1 Move.row < 11}
         }
      then 
         if {And
               {MoveExists ExistingMoves move(row: Move.row column: Move.column - 1 color: blue) false}
               {Not {MoveExists ExistingMoves move(row: Move.row -1 column: Move.column color: blue) false}}
            }
         then
            move(row: Move.row - 1 column: Move.column color: blue)
         elseif 
            {And
               {MoveExists ExistingMoves move(row: Move.row + 1 column: Move.column - 1 color: blue) false}
               {Not {MoveExists ExistingMoves move(row: Move.row +1 column: Move.column  color:blue) false}}
            }
         then
            move(row: Move.row + 1 column: Move.column color: blue)
         else 
            nil
         end
      else
         nil
      end
   end
   
   /*
    * Check if a move is endangered
    * Move1: the first move
    * Move2: the second move
    * EndangeredMove: the move we need to check
    * ExistingMoves: list of existing moves
   */
   fun {CheckEndangered Move1 Move2 EndangeredMove ExistingMoves}
      {And 
         {And 
            {MoveExists ExistingMoves Move1 true}
            {MoveExists ExistingMoves Move2 true}
         }
         {Not {MoveExists ExistingMoves EndangeredMove false}}
      }
   end

   /*
    * Search for neighbors
    * ExistingMoves: list of existing moves
    * Move: The move we are checking
    * Acc: The accumulator list
   */
   fun {FindNeighbors ExistingMoves Move Acc}
      case ExistingMoves of
      X|Xs then
         if {IsNeighbor X Move false} then
            {FindNeighbors Xs Move X|Acc}
         else
            {FindNeighbors Xs Move Acc} 
         end
      [] nil then
         Acc
      end    
   end

   /*
    * Check if two moves are neighbors
    * Move: first move
    * OtherMove: second move
    * SameColor: true if color also has to match   
   */
   fun {IsNeighbor Move OtherMove SameColor}
      if (Move.color == OtherMove.color) == SameColor then
         if {Or
               {And {Number.abs Move.row - OtherMove.row} == 1 Move.column == OtherMove.column}
               {And {Number.abs Move.column - OtherMove.column} == 1 Move.row == OtherMove.row}
            } 
         then
            true
         else 
            if {Or
                  {And Move.row - 1 == OtherMove.row Move.column + 1 == OtherMove.column}
                  {And Move.row + 1 == OtherMove.row Move.column - 1 == OtherMove.column}
               } 
            then
               true 
            else
               false
            end
         end
      else
         false
      end
   end

   
   /*
    * Checks if row and column are in range   
   */
   fun {InRange Row Col}
      {And 
         {And Row >= 1 Col >=1} 
         {And Row =< 11 Col =< 11}
      }
   end

end
